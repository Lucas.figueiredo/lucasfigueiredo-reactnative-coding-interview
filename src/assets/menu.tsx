import React, { memo } from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

const SvgComponent: React.FC<SvgProps> = props => (
  <Svg
    {...props}
    fill="none"
    stroke="currentColor"
    strokeLinecap="round"
    strokeLinejoin="round"
    strokeWidth={2}>
    <Path d="M3 12h18M3 6h18M3 18h18" />
  </Svg>
);
const Memo = memo(SvgComponent);
export { Memo as MenuIcon };
