import {
  DrawerContentComponentProps,
  DrawerContentScrollView,
  DrawerItem,
} from '@react-navigation/drawer';
import React from 'react';
import { useInfiniteQuery } from 'react-query';
import { useNavigation } from '@react-navigation/native';
import { IEmployee } from '../../types/employee';
import {
  DrawerNavigation,
  DrawerRoutes,
  StackRoutes,
} from '../../navigators/paramsList';

type StackItemNavigation = DrawerNavigation<DrawerRoutes.StackNavigator>;

export const DrawerContent: React.FC<DrawerContentComponentProps> = props => {
  const { data } = useInfiniteQuery<{ data: IEmployee[] }>('getPersonsList');

  const { navigate } = useNavigation<StackItemNavigation>();

  const onPress = () => {
    const employeeData = data?.pages[0].data[0];

    if (!employeeData) return;

    navigate(DrawerRoutes.StackNavigator, {
      screen: StackRoutes.EmployeeDetail,
      params: { employee: data?.pages[0].data[0] },
    });
  };

  return (
    <DrawerContentScrollView {...props}>
      <DrawerItem label="Profile" onPress={onPress} />
    </DrawerContentScrollView>
  );
};
