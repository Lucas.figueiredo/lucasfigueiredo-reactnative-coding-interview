import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { ScreenParamsList, StackRoutes } from '../../navigators/paramsList';
import { IEmployee } from '../../types/employee';
import styles from './styles';

type EmployeesStackProp = StackNavigationProp<
  ScreenParamsList,
  StackRoutes.Employees
>;

interface Props {
  item: IEmployee;
}

export const Employee = (props: Props) => {
  const { item } = props;
  const { navigate } = useNavigation<EmployeesStackProp>();

  const goToEmployeeDetail = () => {
    navigate(StackRoutes.EmployeeDetail, { employee: item });
  };

  return (
    <TouchableOpacity style={styles.employeeItem} onPress={goToEmployeeDetail}>
      <View style={styles.employeeInfo}>
        <Text>
          {item.firstname} {item.lastname}
        </Text>
        <Text>{item.email}</Text>
      </View>
    </TouchableOpacity>
  );
};
