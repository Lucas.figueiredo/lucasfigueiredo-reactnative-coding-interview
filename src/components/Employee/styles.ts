import { StyleSheet, Dimensions, Platform } from 'react-native';

const { width } = Dimensions.get('window');

const EmployeeCardSize = Platform.select({
  ios: width / 2,
  android: undefined,
});

export default StyleSheet.create({
  employeeItem: {
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
    width: EmployeeCardSize,
    height: EmployeeCardSize,
  },
  employeeAvatar: {
    height: 48,
    width: 48,
    borderWidth: 1,
    borderRadius: 100,
    marginHorizontal: 10,
  },
  employeeInfo: {
    flex: 1,
    justifyContent: 'space-around',
  },
});
