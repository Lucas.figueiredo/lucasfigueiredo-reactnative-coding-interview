import React from 'react';
import { TouchableOpacity } from 'react-native';
import { HeaderButtonProps } from '@react-navigation/elements';
import { useNavigation } from '@react-navigation/native';
import { DrawerNavigation, DrawerRoutes } from '../../navigators/paramsList';
import { MenuIcon } from '../../assets/menu';
import { styles } from './styles';

type StackItemNavigation = DrawerNavigation<DrawerRoutes.StackNavigator>;

export const HeaderRight: React.FC<HeaderButtonProps> = () => {
  const { toggleDrawer } = useNavigation<StackItemNavigation>();
  return (
    <TouchableOpacity style={styles.root} onPress={toggleDrawer}>
      <MenuIcon color="#6C3ECD" width={24} height={24} />
    </TouchableOpacity>
  );
};
