import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { EmployeeDetailScreen, EmployeesScreen } from '../screens';
import { ScreenParamsList, StackRoutes } from './paramsList';
import { HeaderRight } from '../components/HeaderRight/HeaderRight';

const Stack = createStackNavigator<ScreenParamsList>();

export const StackNavigator: React.FC = () => (
  <Stack.Navigator
    screenOptions={{
      headerTintColor: '#6C3ECD',
      headerTitleAlign: 'center',
    }}>
    <Stack.Screen
      options={{
        headerRight: props => <HeaderRight {...props} />,
      }}
      component={EmployeesScreen}
      name={StackRoutes.Employees}
    />
    <Stack.Screen
      component={EmployeeDetailScreen}
      name={StackRoutes.EmployeeDetail}
    />
  </Stack.Navigator>
);
