import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { DrawerParamsList, DrawerRoutes } from './paramsList';
import { StackNavigator } from './StackNavigator';
import { DrawerContent } from '../components/DrawerContent/DrawerContent';

const Drawer = createDrawerNavigator<DrawerParamsList>();

export function MainNavigator() {
  return (
    <Drawer.Navigator
      initialRouteName={DrawerRoutes.StackNavigator}
      drawerContent={props => <DrawerContent {...props} />}
      screenOptions={{ headerShown: false, drawerPosition: 'right' }}>
      <Drawer.Screen
        name={DrawerRoutes.StackNavigator}
        component={StackNavigator}
      />
    </Drawer.Navigator>
  );
}
