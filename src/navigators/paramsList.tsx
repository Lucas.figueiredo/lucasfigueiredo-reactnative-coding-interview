import {
  CompositeNavigationProp,
  NavigatorScreenParams,
} from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { DrawerNavigationProp } from '@react-navigation/drawer';

import { IEmployee } from '../types/employee';

export enum DrawerRoutes {
  StackNavigator = 'StackNavigator',
}

export type DrawerParamsList = {
  [DrawerRoutes.StackNavigator]: NavigatorScreenParams<ScreenParamsList>;
};

export enum StackRoutes {
  Employees = 'Employees',
  EmployeeDetail = 'EmployeeDetail',
}

export type ScreenParamsList = {
  [StackRoutes.Employees]: undefined;
  [StackRoutes.EmployeeDetail]: { employee: IEmployee };
};

export type DrawerNavigation<T extends DrawerRoutes> = DrawerNavigationProp<
  DrawerParamsList,
  T
>;

export type StackNavigation<T extends StackRoutes> = CompositeNavigationProp<
  StackNavigationProp<ScreenParamsList, T>,
  DrawerNavigation<DrawerRoutes.StackNavigator>
>;
